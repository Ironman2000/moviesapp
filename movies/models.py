from django.db import models

class Movie(models.Model):
	title = models.CharField(max_length=120)
	year = models.PositiveSmallIntegerField(default=2000)
	description = models.TextField(default="")
	imdb = models.DecimalField(max_digits=4, decimal_places=2, null=True, blank=False)
	poster = models.ImageField(upload_to="posters", null=True, blank=True)

	def __str__(self):
		return self.title
