from django.shortcuts import render, redirect, get_object_or_404
from django.http import HttpResponse

from .models import Movie
from .forms import MovieForm

def home_page(request):
	movies = Movie.objects.all()

	context = {
		'movies': movies,
	}

	return render(request, 'index.html', context=context)

def new_movie_page(request):
	form = MovieForm(request.POST or None, request.FILES or None)

	if form.is_valid():
		form.save()
		return redirect(home_page)

	return render(request, 'new_movie.html', {'form': form})

def edit_movie_page(request, id):
	movie = get_object_or_404(Movie, pk=id)
	form = MovieForm(request.POST or None, request.FILES or None, instance=movie)

	if form.is_valid():
		form.save()
		return redirect(home_page)

	return render(request, 'edit_movie.html', {'form': form})
