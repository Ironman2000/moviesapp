from django.urls import path

from .views import home_page, new_movie_page, edit_movie_page

urlpatterns = [
    path('', home_page, name="home_page"),
    path('new_movie/', new_movie_page, name="new_movie"),
    path('edit_movie/<int:id>/', edit_movie_page, name="edit_movie"),
]
